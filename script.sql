DROP TABLE TYPEBAT,PROPRIETAIRE,BATIMENT,CLIENT,LOCATION,PIECE,COMPOSER CASCADE;


CREATE TABLE TYPEBAT (
    numTypeBat char(6) PRIMARY KEY,
    designation varchar(30) NOT NULL
);

CREATE TABLE PROPRIETAIRE (
    numProprio char(5) PRIMARY KEY,
    nom varchar(20) NOT NULL,
    prenom varchar(20),
    numTel varchar(20) NOT NULL CHECK (numTel ~ '[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}'), -- permet de vérifier que le numéro de téléphone est dans le bon format
    entreprise char(1) NOT NULL CHECK (entreprise in ('O','N'))

);

CREATE TABLE BATIMENT (
    numBat char(5) PRIMARY KEY,
    designation varchar(50) NOT NULL,
    superficie numeric(4) NOT NULL,
    prixJ numeric NOT NULL,
    adresse varchar(40) NOT NULL,
    ville varchar(30) NOT NULL,
    numType char(6) NOT NULL REFERENCES TYPEBAT, 
    numProprio char(5) NOT NULL REFERENCES PROPRIETAIRE
);



CREATE TABLE CLIENT (
    numClient char(5) PRIMARY KEY,
    nom varchar(20) NOT NULL,
    prenom varchar(20),
    numTel varchar(20) NOT NULL CHECK (numTel ~ '[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}\.[0-9]{2}'), -- permet de vérifier que le numéro de téléphone est dans le bon format
    statut varchar(20) NOT NULL CHECK (statut IN ('entreprise', 'particulier')),
    adresseFacturation varchar(50) NOT NULL,
    villeFacturation varchar(50) NOT NULL
);


CREATE TABLE LOCATION (
    numBat char(5) REFERENCES BATIMENT,
    numClient char(5) REFERENCES CLIENT,
    nbJour numeric NOT NULL,
    dateLoc date,
    PRIMARY KEY(numBat,numClient)
);


CREATE TABLE PIECE (
    numPiece char(6) PRIMARY KEY,
    designation varchar(20) NOT NULL
);

CREATE TABLE COMPOSER (
    numBat char(5) REFERENCES BATIMENT,
    numPiece char(6) REFERENCES PIECE,
    nbPiece numeric NOT NULL,
    PRIMARY KEY(numBat,numPiece)
);


INSERT INTO TYPEBAT VALUES ('TB0001','salle des fêtes');
INSERT INTO TYPEBAT VALUES ('TB0002','MJC');
INSERT INTO TYPEBAT VALUES ('TB0003','Châlet');
INSERT INTO TYPEBAT VALUES ('TB0004','Maison');


INSERT INTO PROPRIETAIRE VALUES ('P0001','LABORIE','Louis','07.82.98.84.34','N');
INSERT INTO PROPRIETAIRE VALUES ('P0002','MARCILLAC','Jean','06.15.24.89.32','N');
INSERT INTO PROPRIETAIRE VALUES ('P0003','ATOUIL','Malak','07.74.56.87.39','N');
INSERT INTO PROPRIETAIRE VALUES ('P0004','Mairie de Moulins',NULL,'04.70.23.78.45','O');
INSERT INTO PROPRIETAIRE VALUES ('P0005','Mairie de Toulouse',NULL,'05.61.12.31.64','O');



INSERT INTO BATIMENT VALUES ('B0001','Maison de la jeunesse et de la culture de Moulins',150,120,'5 rue du Château','Moulins','TB0002','P0004');
INSERT INTO BATIMENT VALUES ('B0002','Châlet des Sapins',300,600,'8 rue François Mitterand','Clermont-Ferrand','TB0003','P0001');
INSERT INTO BATIMENT VALUES ('B0003','Maison des Merveilles',200,400,'5 Avenue Lionel Jospin','Nice','TB0004','P0002');
INSERT INTO BATIMENT VALUES ('B0004','Salle des fêtes de Toulouse',200,300,'28 Rue de Gironi','Toulouse','TB0001','P0005');
INSERT INTO BATIMENT VALUES ('B0005','Châlet des Flocons',350,700,'6 rue Robert Badinter','Huez','TB0003','P0003');


INSERT INTO CLIENT VALUES ('C0001','JAULT','Jean-Luc','06.69.10.22.51','particulier','6 B Rue des Anglais','Massy');
INSERT INTO CLIENT VALUES ('C0002','DESGRANGES','Thomas','06.14.78.42.30','particulier','15 Avenue de la Libération','Paris');
INSERT INTO CLIENT VALUES ('C0003','GHEBRID','Sami','07.71.75.98.34','particulier','32 Rue Jean Jaurès','Lyon');
INSERT INTO CLIENT VALUES ('C0004','CLUBROOM',NULL,'05.82.89.18.41','entreprise','33 Avenue des Lions','Lyon');
INSERT INTO CLIENT VALUES ('C0005','DUCROU',NULL,'04.82.65.38.12','entreprise','33 rue des Vainqueurs','Metz');
INSERT INTO CLIENT VALUES ('C0006','ROCKSTAR GAMES',NULL,'01.65.98.32.48','entreprise','9 Boulevard de la Révolution','Toulouse');


INSERT INTO LOCATION VALUES ('B0001','C0001',1,'2022-11-24');
INSERT INTO LOCATION VALUES ('B0001','C0004',3,'2022-11-01');
INSERT INTO LOCATION VALUES ('B0002','C0003',2,'2022-11-15');
INSERT INTO LOCATION VALUES ('B0002','C0002',1,'2022-12-06');
INSERT INTO LOCATION VALUES ('B0004','C0005',4,'2022-12-02');
INSERT INTO LOCATION VALUES ('B0005','C0006',2,'2022-12-01');




INSERT INTO PIECE VALUES ('PI0001','Chambre');
INSERT INTO PIECE VALUES ('PI0002','Salle de bain');
INSERT INTO PIECE VALUES ('PI0003','Salle à manger');
INSERT INTO PIECE VALUES ('PI0004','Toilettes');
INSERT INTO PIECE VALUES ('PI0005','Salle de vie');


INSERT INTO COMPOSER VALUES('B0002','PI0001',5);
INSERT INTO COMPOSER VALUES('B0002','PI0002',2);
INSERT INTO COMPOSER VALUES('B0002','PI0003',1);
INSERT INTO COMPOSER VALUES('B0002','PI0005',1);
INSERT INTO COMPOSER VALUES('B0001','PI0005',1);
INSERT INTO COMPOSER VALUES('B0001','PI0004',2);
INSERT INTO COMPOSER VALUES('B0003','PI0001',2);
INSERT INTO COMPOSER VALUES('B0003','PI0002',2);
INSERT INTO COMPOSER VALUES('B0003','PI0003',1);
INSERT INTO COMPOSER VALUES('B0003','PI0004',2);
INSERT INTO COMPOSER VALUES('B0003','PI0005',1);
INSERT INTO COMPOSER VALUES('B0004','PI0005',1);
INSERT INTO COMPOSER VALUES('B0004','PI0004',4);
INSERT INTO COMPOSER VALUES('B0005','PI0001',5);
INSERT INTO COMPOSER VALUES('B0005','PI0002',2);
INSERT INTO COMPOSER VALUES('B0005','PI0003',1);
INSERT INTO COMPOSER VALUES('B0005','PI0005',1);


-- Question 1 : Y a-t-il des articles jamais loués ? 
SELECT * FROM BATIMENT WHERE numBat NOT IN (SELECT l.numBat FROM LOCATION l,BATIMENT b WHERE l.numBat = b.numBat);

-- Question 2 : Une action commerciale a été menée la semaine dernière sur les clients de type 'entreprise'. L’entreprise souhaiterait savoir s’il y a eu un effet sur les locations.
SELECT count(l.numClient) AS NB_CLIENTS_ACTION FROM LOCATION l, CLIENT c WHERE l.numClient = c.numClient AND c.statut = 'entreprise' AND l.dateLoc >= (current_date - 7);

-- Question 3 : Existe-t-il des produits qui n’ont jamais été loué ? (même réponse que la première

-- Question 4 : Le comptable de l’entreprise sollicite votre aide pour remplir la déclaration de TVA pour le dernier mois. En fonction des types de clients, il a besoin du montant des locations correspondant aux deux ventilations suivantes :
SELECT l.nbJour * b.prixJ AS VENTES_CLIENTS_PARTICULIERS FROM LOCATION l, BATIMENT b, CLIENT c WHERE l.numBat = b.numBat AND l.numClient = c.numClient AND c.statut = 'particulier';

SELECT l.nbJour * b.prixJ AS VENTES_CLIENTS_ENTREPRISES FROM LOCATION l, BATIMENT b, CLIENT c WHERE l.numBat = b.numBat AND l.numClient = c.numClient AND c.statut = 'entreprise';